package searchengine;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class Grader
{
	Keyword[] kws;
	int kwCnt;
	public Grader()
	{
		
	}
	public void setKeywords() throws IOException
	{
		String fileName = "Keywords.txt";
		Scanner scanf = new Scanner(new File(fileName));
		kwCnt = scanf.nextInt();
		System.out.println(kwCnt);
		kws = new Keyword[kwCnt];
		int i;
		for(i=0; i<kwCnt; i++)
		{
			kws[i] = new Keyword(scanf.next(), scanf.nextDouble());
			System.out.print(kws[i].name+" "+kws[i].weight+"\n");
		}
		scanf.close();
	}
	public int count(String t, String p)
	{
		t = t.toLowerCase();
		p = p.toLowerCase();
		int ans = 0, n = t.length(), m = p.length(), i, j;
		int[] f = new int[65536];
		for(i=m-1; i>=0; i--)
		{
			if(f[p.charAt(i)]==0)
			{
				f[p.charAt(i)] = i+1;
			}
		}
		
		i = j = m-1;
		while(i<n)
		{
			char a = t.charAt(i), b = p.charAt(j);
			//System.out.println(i+" "+j+" "+a+" "+b);
			if(a==b)
				if(j==0)
				{
					ans++;
					j = m-1;
					i += m;
					
				}
				else
				{
					i--;
					j--;
				}
			else
			{
				i = i+m-(j<f[a]?j:f[a]);
				j = m-1;
			}
		}
		return ans;
	}
	public void grade(Website w)
	{
		Webpage home = w.home;
		dfs(home);
		w.score = home.score;
	}
	private void dfs(Webpage v)
	{
		int i;
		for(i=0; i<kwCnt; i++)
		{
			v.score += kws[i].weight*count(v.content, kws[i].name);
		}
		for(i=0; i<v.subpageCnt; i++)
		{
			dfs(v.subpage[i]);
			v.score += v.subpage[i].score;
		}
	}
}
