package searchengine;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class SearchEngine
{
	Website[] ws;
	String userKeyword;
	int wsCnt;
	final int GOOGLE_RESULT_LIMIT = 10;
	public SearchEngine(String uk)
	{
		wsCnt = 0;
		ws = new Website[100];
		userKeyword = uk;
	}
	public void start() throws Exception
	{
		getGoogleResult();
		createWebsites();
		System.out.print("Websites Created\n");
		scoreWebsites();
		//System.exit(0);
	}
	public void scoreWebsites() throws IOException
	{
		int i, j, k;
		double mm;
		Website t;
		Grader g = new Grader();
		g.setKeywords();
		for(i=0; i<GOOGLE_RESULT_LIMIT; i++)
		{
			g.grade(ws[i]);
		}
		for(i=wsCnt-1; i>=0; i--)
		{
			mm = 1000000000.0;
			k = i;
			for(j=0; j<=i; j++)
			{
				if(ws[j].score<mm)
				{
					mm = ws[j].score;
					k = j;
				}
			}
			t = ws[i];
			ws[i] = ws[k];
			ws[k] = t;
		}
		System.out.println("===============Result===============");
		for(i=0; i<wsCnt; i++)
		{
			System.out.println((i+1)+" "+ws[i].name+" "+ws[i].url);
		}
		if(Test.mode==1)
		{
			OutputStreamWriter osw = new OutputStreamWriter(
				new FileOutputStream(new File(Test.fileName+".txt")));
			for(i=0; i<wsCnt; i++)
			{
				osw.write(URLEncoder.encode(
					ws[i].name+","+ws[i].url+","+ws[i].score+"\n", "UTF-8"));
			}
			osw.flush();
			osw.close();
		}
	}
	public void createWebsites() throws IOException, InterruptedException
	{
		int i;
		for(i=0; i<wsCnt; i++)
		{
			System.out.print(ws[i].name+" "+ws[i].url+"\n");
			ws[i].home.addSubPages();
		}
	}
	public void getGoogleResult() throws Exception
	{
		String line, content = "", title, url;
		String s;
		if(Test.mode ==1)
			s = userKeyword+"%2Fdownload";
		else 
			s = URLEncoder.encode(userKeyword+" download", "UTF-8");
		URLConnection uc = new URL("http://www.google.com.tw/search?q=" +
			s + "&num="+GOOGLE_RESULT_LIMIT+"&oe=utf8").openConnection();
		uc.setRequestProperty("User-agent", "Chrome");
		BufferedReader br = new BufferedReader(
			new InputStreamReader(uc.getInputStream(), "utf-8"));
		while((line=br.readLine()) != null)
		{
			content += line +"\n";
		}
		Document gr = Jsoup.parse(content);
		Elements eles = gr.select("li.g");
		
		int i, z=0;
		
		for(Element e:eles)
		{
			try
			{
				title = e.select("h3.r").get(0).text();
				url = getTrueURL(e.select("h3.r").get(0).select("a").attr("href"));
				System.out.print((++z)+" "+title+" "+url+"\n");
				ws[wsCnt++] = new Website(url, title);
			}
			catch(Exception ee){}
		}
		ExecutorService es = Executors.newFixedThreadPool(15);
		for(i=0; i<wsCnt; i++)
			es.execute(ws[i].home);
		es.shutdown();
		while(!es.isTerminated())
		{
			try
			{
				Thread.sleep(1000);
				System.out.print("waiting...\n");
			}
			catch(Exception ee){}
		}
		System.out.print("Finished\n");
		
	}
	private String getTrueURL(String s) throws Exception
	{
		return URLDecoder.decode(s.substring(7, s.lastIndexOf("&sa=")), "UTF-8");
	}
}
