package searchengine;

import java.util.Scanner;

public class Test
{
	static int mode;
	static String fileName;
	public static void main(String[] args) throws Exception
	{
		int i,n = args.length;
		if(n>0)
			System.out.println(n);
		for(i=0;i<n;i++)
		{
			System.out.print(args[i]+" ");
		}
		if(n>2 && args[1].equals("-file"))
		{
			mode = 1;
			fileName = args[2];
		}
		else
			mode = 0;
		if(n>0)
		{
			SearchEngine se = new SearchEngine(args[0]);
			se.start();
			return;
		}
		Scanner scanf = new Scanner(System.in);
		SearchEngine se = new SearchEngine(scanf.next());
		se.start();
		scanf.close();
	}
}

