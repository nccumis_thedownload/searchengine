package searchengine;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Webpage implements Runnable
{
	public Webpage[] subpage;
	public int subpageCnt, depth;
	public String title, url, content;
	public double score;
	public Webpage(String s, String t) throws IOException
	{
		subpageCnt = 0;
		url = s;
		title = t;
		score = 0.0;
		depth = 100;
		subpage = new Webpage[500];
		content = "";
	}
	public void run()
	{
		try
		{
			getContent();
		} 
		catch(IOException e){return;}
	}
	private void getContent() throws IOException
	{
		String line;
		URL u = new URL(url);
		URLConnection uc = u.openConnection();
		uc.setRequestProperty("User-agent", "Chrome");
		uc.setReadTimeout(1000);
		BufferedReader br = new BufferedReader(
			new InputStreamReader(uc.getInputStream(), "utf-8"));
		line = br.readLine();
		if(line!=null){ 
			content += line+"\n";
			if(!uc.getContentType().startsWith("text/html"))
				return; 
		}
		while((line=br.readLine()) != null)
		{
			content += line + "\n";
		}
		//System.out.print(url+"\n");
	}
	public void addSubPages() throws IOException, InterruptedException
	{
		Document doc = Jsoup.parse(content);
		Elements eles = doc.select("a");
		//System.out.print(content+"\n");
		for(Element e:eles)
		{
			try
			{
				String s = e.attr("href");
				if(s.charAt(0) == '#' || 
				   s.startsWith("http://") ||
				   s.startsWith("https://") ||
				   s.startsWith("javascript:") || 
				   s.startsWith("mailto:") ||
				   s.startsWith("magnet")
				)
					continue;
				s = URLConverter(s);
				subpage[subpageCnt++] = new Webpage(s, e.text());
			}
			catch(Exception ee){}
		}
		int i;
		ExecutorService es = Executors.newFixedThreadPool(20);
		System.out.print(url+" has "+subpageCnt+" subpage(s)\n");
		for(i=0;i<subpageCnt; i++)
		{
			es.execute(subpage[i]);
		}
		es.shutdown();
		i=0;
		while(!es.isTerminated() & i<10)
		{
			try
			{
				i++;
				Thread.sleep(1000);
				System.out.print("waiting... "+url+"\n");
			}
			catch(Exception ee){}
		}
		es.shutdownNow();
		System.out.print(url+"'s subpages has been created\n");
		/*if(!es.awaitTermination(10, TimeUnit.SECONDS))
			es.shutdownNow();*/
	}
	private String URLConverter(String s)
	{
		if(s.charAt(0) == '.')
			return url + s.substring(2);
		if(s.startsWith("http://") || s.startsWith("https://"))
			return s;
		if(url.charAt(4) == 's')
			return url.substring(0, url.indexOf('/', 8)) + s; 
		return url.substring(0, url.indexOf('/', 7)) + s;
	}
}
