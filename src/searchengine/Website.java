package searchengine;

import java.io.IOException;

public class Website
{
	String name, url;
	double score;
	public Webpage home;
	public Website(String url, String name) throws IOException
	{
		this.url = url;
		this.name = name;
		score = 0.0;
		home = new Webpage(url, name);
	}
}
